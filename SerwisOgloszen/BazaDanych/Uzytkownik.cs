//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SerwisOgloszen.BazaDanych
{
    using System;
    using System.Collections.Generic;
    
    public partial class Uzytkownik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Uzytkownik()
        {
            this.Ogloszenie = new HashSet<Ogloszenie>();
            this.Wiadomosc = new HashSet<Wiadomosc>();
            this.Wiadomosc1 = new HashSet<Wiadomosc>();
        }
    
        public long Id { get; set; }
        public string Login { get; set; }
        public string Haslo { get; set; }
        public byte Rola { get; set; }
        public bool CzyUsuniety { get; set; }
        public string Sol { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ogloszenie> Ogloszenie { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Wiadomosc> Wiadomosc { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Wiadomosc> Wiadomosc1 { get; set; }
    }
}
