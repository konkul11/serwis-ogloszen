﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SerwisOgloszen.Enums
{
    public enum RolaUzytkownika
    {
        Uzytkownik = 1,
        Administrator = 2
    }
}